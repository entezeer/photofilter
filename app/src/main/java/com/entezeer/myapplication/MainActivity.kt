package com.entezeer.myapplication

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.widget.Toast
import java.io.FileNotFoundException
import android.widget.Button
import com.gigamole.infinitecycleviewpager.HorizontalInfiniteCycleViewPager


class MainActivity : AppCompatActivity() {

    private val SELECT_PICTURE = 1
    private var dowloadButton: FloatingActionButton? = null
    private var loadImage: Drawable? = null
    private var negativeButton: Button? = null
    private var definitionButton: Button? = null
    private val imageProcessing = ImageProcessing()
    private var listImages: MutableList<Bitmap> = ArrayList()
    private var cyclePager: HorizontalInfiniteCycleViewPager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkPermission()

        init()
    }

    private fun checkPermission() {
        if (PermissionUtil.isPermissionGranted(this)) {
            PermissionUtil.makeRequest(this)
        } else PermissionUtil.showPermissionRequestRationale(this)
    }

    private fun init() {
        cyclePager = findViewById(R.id.horizontal_cycle)

        negativeButton = findViewById(R.id.set_negative_button)
        negativeButton?.setOnClickListener {
            if (loadImage != null) {
                listImages.add(imageProcessing.setNegativeFilter(loadImage!!)!!)
                cyclePager?.adapter = CardAdapter(listImages, this)
            } else Toast.makeText(this, "Please select image", Toast.LENGTH_LONG).show()
        }

        definitionButton = findViewById(R.id.set_definition_button)
        definitionButton?.setOnClickListener {
            if (loadImage != null) {
                listImages.add(imageProcessing.setDefinitionFilter(loadImage!!)!!)
                cyclePager?.adapter = CardAdapter(listImages, this)
            } else Toast.makeText(this, "Please select image", Toast.LENGTH_LONG).show()
        }

        dowloadButton = findViewById(R.id.download_fab)
        dowloadButton?.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(
                Intent.createChooser(
                    intent,
                    "Select Picture"
                ), SELECT_PICTURE
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {

        super.onActivityResult(requestCode, resultCode, intent)
        if (requestCode == 1) {

            if (intent != null && resultCode == Activity.RESULT_OK) {

                val selectedImage = intent.data
                try {
                    val inputStream = contentResolver.openInputStream(selectedImage)
                    loadImage = Drawable.createFromStream(inputStream, selectedImage.toString())
                    listImages.add((loadImage as BitmapDrawable).bitmap)
                    cyclePager?.adapter = CardAdapter(listImages,this)
                    Toast.makeText(this, "success", Toast.LENGTH_LONG).show()

                } catch (e: FileNotFoundException) {
                    Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
                }

            }
        }
    }


}

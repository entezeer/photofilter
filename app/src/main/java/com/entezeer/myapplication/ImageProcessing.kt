package com.entezeer.myapplication

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable

/**
 * Created by entezeer on 25.02.19
 */
class ImageProcessing {
    fun setNegativeFilter(drawable: Drawable): Bitmap? {

        val bitmapImage = (drawable as BitmapDrawable).bitmap
        val finalImage = Bitmap.createBitmap(bitmapImage.width, bitmapImage.height, bitmapImage.config)

        val width = bitmapImage.width
        val height = bitmapImage.height

        for (i in 0 until width) {
            for (j in 0 until height) {
                val pixelColor = bitmapImage.getPixel(i, j)

                val A = Color.alpha(pixelColor)
                val R = 255 - Color.red(pixelColor)
                val G = 255 - Color.green(pixelColor)
                val B = 255 - Color.blue(pixelColor)

                finalImage.setPixel(i, j, Color.argb(A, R, G, B))
            }
        }
        return finalImage
    }

    fun setDefinitionFilter(drawable: Drawable): Bitmap? {
        val bitmapImage = (drawable as BitmapDrawable).bitmap
        val finalImage = Bitmap.createBitmap(bitmapImage.width, bitmapImage.height, bitmapImage.config)

        val width = bitmapImage.width
        val height = bitmapImage.height

        for (i in 1 until width - 1) {
            for (j in 1 until height - 1) {
                val color1 = bitmapImage.getPixel(i - 1, j - 1)
                val color2 = bitmapImage.getPixel(i, j - 1)
                val color3 = bitmapImage.getPixel(i + 1, j - 1)
                val color4 = bitmapImage.getPixel(i - 1, j)
                val color5 = bitmapImage.getPixel(i, j)
                val color6 = bitmapImage.getPixel(i + 1, j)
                val color7 = bitmapImage.getPixel(i - 1, j + 1)
                val color8 = bitmapImage.getPixel(i, j + 1)
                val color9 = bitmapImage.getPixel(i + 1, j + 1)


                var A = (Color.alpha(color1)
                        + Color.alpha(color2)
                        + Color.alpha(color3)
                        + Color.alpha(color4)
                        + Color.alpha(color6)
                        + Color.alpha(color7)
                        + Color.alpha(color8)
                        + Color.alpha(color9)) * (-1) + Color.alpha(color5) * 9

                var R = (Color.red(color1)
                        + Color.red(color2)
                        + Color.red(color3)
                        + Color.red(color4)
                        + Color.red(color6)
                        + Color.red(color7)
                        + Color.red(color8)
                        + Color.red(color9)) * (-1) + Color.red(color5) * 9

                var G = (Color.green(color1)
                        + Color.green(color2)
                        + Color.green(color3)
                        + Color.green(color4)
                        + Color.green(color6)
                        + Color.green(color7)
                        + Color.green(color8)
                        + Color.green(color9)) * (-1) + Color.green(color5) * 9
                var B = (Color.blue(color1)
                        + Color.blue(color2)
                        + Color.blue(color3)
                        + Color.blue(color4)
                        + Color.blue(color6)
                        + Color.blue(color7)
                        + Color.blue(color8)
                        + Color.blue(color9)) * (-1) + Color.blue(color5) * 9
                if (A > 255) {
                    A = 255
                }
                if (R > 255) {
                    R = 255
                }
                if (G > 255) {
                    G = 255
                }
                if (B > 255) {
                    B = 255
                }

                if (A < 0) {
                    A = 0
                }
                if (R < 0) {
                    R = 0
                }
                if (G < 0) {
                    G = 0
                }
                if (B < 0) {
                    B = 0
                }
                finalImage.setPixel(i, j, Color.argb(A, R, G, B))
            }
        }
        return finalImage
    }

}
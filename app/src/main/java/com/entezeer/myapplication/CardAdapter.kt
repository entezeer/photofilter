package com.entezeer.myapplication

import android.content.Context
import android.graphics.Bitmap
import android.support.design.widget.FloatingActionButton
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

/**
 * Created by entezeer on 20.02.19
 */

class CardAdapter(var listImages: MutableList<Bitmap>, context: Context) : PagerAdapter() {

    var layoutInflater: LayoutInflater = LayoutInflater.from(context)


    override fun isViewFromObject(view: View, any: Any): Boolean {
        return view == any
    }

    override fun getCount(): Int {
        return listImages.size
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view: View = layoutInflater.inflate(R.layout.card_item,container,false)
        val imageView = view.findViewById<ImageView>(R.id.card_image)
        imageView.setImageBitmap(listImages[position])
        container.addView(view)
        return view
    }

    fun remove(position: Int) {
        listImages.removeAt(position)
        notifyDataSetChanged()
    }
}
package com.entezeer.myapplication

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat

import android.support.v4.app.ActivityCompat.requestPermissions


/**
 * Created by entezeer on 24.02.19
 */
object PermissionUtil {
    const val REQUEST_CODE = 1

    fun makeRequest(context: Activity) {
        requestPermissions(context, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_CODE)
    }

    fun isPermissionGranted(context: Context) = ActivityCompat.checkSelfPermission(
        context,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    ) != PackageManager.PERMISSION_GRANTED

    fun showPermissionRequestRationale(context: Activity) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(context,Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            makeRequest(context)
        }
    }
}